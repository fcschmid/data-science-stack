# Overview <a name="overview"></a>

This tutorial shows how a typical Data Science Stack can be deployed on a container orchestration using Docker and Docker Compose. 

If you need to install a container orchestration first, you can follow this Tutorial: [https://gitlab.com/fcschmid/container-orc-setup](https://gitlab.com/fcschmid/container-orc-setup)

A first setup for storage, processing and developing data science use cases is descriebed by Richard Buractaon. 
This setup represents a Kappa architecture used for processing streaming data. You can realize real-time and batch processing tasks with this single technology stack. 
It uses Jupyter or Polynote as notebook, Spark as Standalone Cluster for processing, a MYSQL database for storing and MLFLow for tracking:
[https://github.com/RBuractaon/docker-spark-jupyter-mlflow-hive-metastore](https://github.com/RBuractaon/docker-spark-jupyter-mlflow-hive-metastore)

An more complex big data pipeline is represented by the Lambda architecure. 
It uses three distinct layers to perform servicing issues, stream processing and batch processing tasks.
The follwoing example uses Hadoop for storage and processing, Spark for real-time or batch processing, and Kafka for data ingestion and real-time processing. For the purpose of a Data Science Stack, the following open source services are used:

interactive development environment for notebooks, code, and data : JupyterLab
storage and distributed data processing: hadoop
real-time or batch processing: spark
data ingestion and real-time processing: kafka

An example for this stack is described by Marco Russo:
[https://github.com/marcusRB/final-project-cluster-on-docker] (https://github.com/marcusRB/final-project-cluster-on-docker)

<!-- [Services](1_Services.md) describes the installation of the service. All necessary configuration files are placed in the Data folder. -->

